FROM ruby:2.5
RUN apt-get update -qq && apt-get install -y nodejs postgresql-client \
  g++ qt5-default libqt5webkit5-dev gstreamer1.0-plugins-base \
  gstreamer1.0-tools gstreamer1.0-x xvfb
RUN mkdir /url_shortner
WORKDIR /url_shortner
COPY Gemfile /url_shortner/Gemfile
COPY Gemfile.lock /url_shortner/Gemfile.lock
RUN bundle install
COPY . /url_shortner

# Add a script to be executed every time the container starts.
COPY entrypoint.sh /usr/bin/
RUN chmod +x /usr/bin/entrypoint.sh
ENTRYPOINT ["entrypoint.sh"]
EXPOSE 4000

# Start the main process.
CMD ["rails", "server", "-b", "0.0.0.0"]
