Rails.application.routes.draw do
  get '/stats', to: 'stats#index'
  get '/shorten', to: 'url_details#index'
  get '/:short_url', to: 'url_details#show'
  root to: 'url_details#show'

  post '/create', to: 'url_details#create', as: 'url_details'
end
