class CreateStats < ActiveRecord::Migration[5.2]
  def change
    create_table :stats do |t|
      t.references :url_detail
      t.integer :clicks
      t.string :ip_address
      t.string :country

      t.timestamps
    end
  end
end
