class CreateUrlDetails < ActiveRecord::Migration[5.2]
  def change
    create_table :url_details do |t|
      t.string :short_url, limit: 5
      t.text :long_url
      t.datetime :expires_at

      t.timestamps
    end
    add_index :url_details, :short_url, unique: true
    add_index :url_details, %i[short_url expires_at]
  end
end
