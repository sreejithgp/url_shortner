class RemoveClickFromStatsTable < ActiveRecord::Migration[5.2]
  def up
    remove_column :stats, :clicks
  end

  def down
    add_column :stats, :clicks, :integer
  end
end
