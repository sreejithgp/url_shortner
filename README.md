# README

## Up and running

```
docker-compose up
```

<http://localhost:4000/shorten>

## TODO
- Validations
- Pagination for stats page
- Redis caching

## Issues

This uses MD5 hash to create 5 chars long shortened url. Character limit is low in this case [0-9a-f] this can lead to collision when scaling. A base62 approach is usually preferred.

Write rate is low for this service as users creating shortened url are lesser as compared to using the short url to redirect to long url. Hence read rate is important for fetching long url so a write for analytic operations can be expensive so this can be moved to a background job and another table can be maintained to keep track of cummulative date rather than using a join query.
