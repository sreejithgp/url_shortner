require 'rails_helper'

RSpec.describe StatsHelper, type: :helper do
  include ActionView::Helpers
  describe '#top_countries' do
    it 'returns correct string' do
      array = %w[India USA China Brazil India China India Brazil England Peru China India]
      expect(helper.top_countries(array)).to eq('India, China, Brazil')
    end
  end
end
