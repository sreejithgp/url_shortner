FactoryBot.define do
  factory(:stat) do
    url_detail
    ip_address { Faker::Internet.ip_v4_address }
    country { Faker::Address.country }
  end
end
