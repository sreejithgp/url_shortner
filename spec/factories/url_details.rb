FactoryBot.define do
  factory(:url_detail) do
    short_url { rand(99999) }
    long_url { Faker::Internet.url }
    expires_at { Time.now + 30.days }
  end
end
