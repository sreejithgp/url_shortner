require 'rails_helper'

RSpec.describe UrlDetailsController, type: :controller do
  describe 'GET #index' do
    it 'returns http success' do
      get :index
      expect(response).to have_http_status(:success)
    end
  end

  describe 'POST #create' do
    let(:data) { { url_detail: { long_url: 'http://www.google.com' } } }
    it 'creates an new entry' do
      expect { post :create, params: data, format: 'js' }
        .to change(UrlDetail, :count)
    end
  end

  describe 'GET #show' do
    it 'redirects to correct long_url' do
      short_url = FactoryBot.create(:url_detail, long_url: 'http://www.google.com').short_url
      expect { get :show, params: { short_url: short_url } }
        .to change(Stat, :count)
      expect(response).to redirect_to('http://www.google.com')
    end

    it 'returns 404 for incorrect short_url' do
      get :show, params: { short_url: 'd2ac3' }
      expect(response).to have_http_status(404)
    end

    it 'returns 404 for expired url' do
      short_url = FactoryBot.create(:url_detail, expires_at: 5.day.ago).short_url
      get :show, params: { short_url: short_url }
      expect(response).to have_http_status(404)
    end

    it 'caches short url' do
      short_url = FactoryBot.create(:url_detail, expires_at: 5.day.ago).short_url
      expect(UrlDetail).to receive(:find_by_short_url).once
      get :show, params: { short_url: short_url }
      get :show, params: { short_url: short_url }
      get :show, params: { short_url: short_url }
    end
  end
end
