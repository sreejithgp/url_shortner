require 'rails_helper'

RSpec.describe ShortnerService do
  describe '#shorten' do
    subject { described_class.new(long_url: 'http://www.google.com') }
    it 'shortens url to 5 chars' do
      expect(subject.shorten.length).to eq(5)
    end

    it 'generates uniq chars each time' do
      expect(subject.shorten).to_not eq(subject.shorten)
    end

    it 'checks table for uniq short_url' do
      short_url = FactoryBot.create(:url_detail).short_url
      allow(Digest::MD5).to receive_message_chain(:hexdigest, :[])
        .and_return(short_url, 'a23ac')
      expect(subject.shorten).to eq('a23ac')
    end
  end
end
