require 'rails_helper'

RSpec.describe 'Stats', type: :feature do
  it 'shows the correct stats' do
    urls = FactoryBot.create_list(:url_detail, 3)
    hash = {}
    urls.each do |url|
      s = FactoryBot.create_list(:stat, rand(20), url_detail: url)
      hash[url.id] ||= Hash.new(0)
      s.each do |ss|
        hash[url.id]['count'] += 1
        hash[url.id][ss.country] += 1
      end
    end
    top_countries = hash[urls.first.id].sort_by { |_k, v| v }.reverse[0, 4].to_h
    count = top_countries.delete('count').to_i
    visit '/stats'
    expect(page).to have_selector('.stats', count: 3)
    expect(page).to have_content(urls.first.long_url)
    expect(page).to have_content(urls.first.short_url)
    expect(page).to have_content("Total Clicks: #{count}")
    expect(page).to have_content("Top Countries: #{top_countries.keys.join(', ')}")
  end
end
