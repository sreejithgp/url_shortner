require 'rails_helper'

RSpec.describe 'Url Shortening', type: :feature do
  describe 'Creation' do
    it 'returns the correct page' do
      visit '/shorten'
      expect(page).to have_content('enter your URL')
    end

    it 'creates shortened url', js: true do
      visit '/shorten'
      fill_in 'url_detail_long_url', with: 'http://www.google.com'
      click_button 'Shorten'
      expect(page).to have_css('div#shortened-url', text: "Here's your shortened URL")
      expect(page).to have_css('div#shortened-url', text: /127\.0\.0\.1:\d+\/.{5}/)
    end
  end
end
