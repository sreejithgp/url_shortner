module StatsHelper
  def top_countries(countries)
    country_hash = Hash.new(0)
    countries.each { |country| country_hash[country] += 1 }
    country_hash.sort_by { |_k, v| v }.reverse[0, 3].to_h.keys.join(', ')
  end
end
