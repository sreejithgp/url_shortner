class ShortnerService
  LENGTH = 5

  def initialize(long_url:)
    @long_url = long_url
  end

  def shorten
    short_url = generate_string
    while UrlDetail.exists?(short_url: short_url)
      short_url = generate_string
    end
    short_url
  end

  def generate_string
    Digest::MD5.hexdigest(@long_url + rand.to_s)[rand(27), LENGTH]
  end
end
