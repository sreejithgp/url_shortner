class StatsController < ApplicationController
  def index
    @stats = UrlDetail.left_joins(:stats)
                      .select('url_details.*, count(stats.*) as stat_count, array_agg(stats.country) as countries')
                      .group('url_details.id')
  end
end
