class UrlDetailsController < ApplicationController
  def index
    @url_detail = UrlDetail.new
  end

  def create
    short_url = ShortnerService.new(long_url: url_params[:long_url]).shorten
    @url_detail = UrlDetail.new(url_params).tap do |url|
      url.short_url = short_url
      url.expires_at = Time.now + 30.days
    end
    @url_detail.save
  end

  def show
    @url_detail = UrlDetail.cached_find_by_short_url(params[:short_url])

    not_found && return if @url_detail.blank? || @url_detail.expires_at < Time.now

    Stat.create(url_detail: @url_detail,
                ip_address: request.ip,
                country: request.location.country)

    redirect_to @url_detail.long_url
  end

  private

  def url_params
    params.require(:url_detail).permit(:long_url)
  end
end
