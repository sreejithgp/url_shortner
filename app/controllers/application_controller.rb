class ApplicationController < ActionController::Base
  def not_found
    respond_to do |format|
      format.html do
        render file: "#{Rails.root}/public/404",
               layout: false, status: :not_found
      end
    end
  end
end
