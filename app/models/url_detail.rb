class UrlDetail < ApplicationRecord
  has_many :stats
  validates_presence_of :short_url
  validates_uniqueness_of :short_url

  def self.cached_find_by_short_url(url)
    Rails.cache.fetch(url, expires_in: 1.day) do
      find_by_short_url(url)
    end
  end
end
